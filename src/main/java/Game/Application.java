package Game;

import Game.controller.impl.CommentControllerImpl;
import Game.controller.impl.FolderControllerImpl;
import Game.controller.impl.SerializeControllerImpl;
import Game.service.impl.GetCommentImpl;
import Game.service.impl.GetFolderImpl;
import Game.service.impl.SerializationImpl;
import Game.view.Menu;

public class Application {
    public static void main(String[] args) {

        new Menu(
                new SerializeControllerImpl(
                        new SerializationImpl()
                ),
                new CommentControllerImpl(
                        new GetCommentImpl()
                ),
                new FolderControllerImpl(
                        new GetFolderImpl()
                )
        );
    }
}
