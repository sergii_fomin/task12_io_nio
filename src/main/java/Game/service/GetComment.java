package Game.service;

import java.util.List;

public interface GetComment {

    List<String> getCommentsFromFile(String filename);
}
