package Game.service.impl;

import Game.service.GetFolder;

import java.io.File;
import java.util.Arrays;
import java.util.List;

public class GetFolderImpl implements GetFolder {

    @Override
    public List<File> getFolderContent(String path) {
        File f = new File(path);
        return Arrays.asList(f.listFiles());
    }
}
