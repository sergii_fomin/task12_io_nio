package Game.service;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.*;
import java.net.InetSocketAddress;
import java.nio.ByteBuffer;
import java.nio.channels.SocketChannel;

public class Client {

    private static final String IP = "localhost";
    private static final int PORT = 9000;
    private static Logger logger = LogManager.getLogger(Client.class);

    public static void main(String[] args) throws IOException {

        InetSocketAddress hostAddress = new InetSocketAddress(IP, PORT);
        SocketChannel client = SocketChannel.open(hostAddress);
        System.out.println("Client started... ");
        System.out.println("Type your message: ");
        Runnable write = () -> {
            while (true) {
                BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
                String line = null;
                try {
                    line = reader.readLine();
                } catch (IOException e) {
                    e.printStackTrace();
                }
                byte[] message = line.getBytes();
                ByteBuffer buffer = ByteBuffer.wrap(message);
                try {
                    client.write(buffer);
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        };

        Runnable read = () -> {
            while (true) {
                ByteBuffer response = ByteBuffer.allocate(51200);
                try {
                    client.read(response);
                } catch (IOException e) {
                    e.printStackTrace();
                }
                String responseMessage = new String(response.array());
                System.out.println(responseMessage);
            }
        };
        new Thread(write).start();
        new Thread(read).start();
    }
}
