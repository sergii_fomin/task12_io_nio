package Game.service;

public interface Serialization {

    void writeObject(String path, Object object);
    Object readObject(String path);
}
