package Game.service;

import java.io.File;
import java.util.List;

public interface GetFolder {

    List<File> getFolderContent(String path);
}
