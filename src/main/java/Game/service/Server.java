package Game.service;

import java.io.IOException;
import java.net.InetSocketAddress;
import java.net.Socket;
import java.net.SocketAddress;
import java.nio.ByteBuffer;
import java.nio.channels.SelectionKey;
import java.nio.channels.Selector;
import java.nio.channels.ServerSocketChannel;
import java.nio.channels.SocketChannel;
import java.util.*;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class Server {

    private Selector selector;
    private InetSocketAddress address;
    private Set<SocketChannel> dataMapper;
    private static Logger logger = LogManager.getLogger(Server.class);

    public static void main(String[] args) {
        Runnable server = () -> {
            try {
                new Server("localhost", 9000).start();
            } catch (IOException e) {
                e.printStackTrace();
            }
        };
        new Thread(server).start();
    }

    public Server(String host, int port) {
        address = new InetSocketAddress(host, port);
        dataMapper = new HashSet<>();
    }

    private void start() throws IOException {
        this.selector = Selector.open();
        ServerSocketChannel serverSocketChannel = ServerSocketChannel.open();
        serverSocketChannel.configureBlocking(false);
        serverSocketChannel.socket().bind(address);
        serverSocketChannel.register(this.selector, SelectionKey.OP_ACCEPT);
        logger.info("Server starting... ");

        while (true) {
            this.selector.select();
            Iterator<SelectionKey> keys = this.selector.selectedKeys().iterator();
            while (keys.hasNext()) {
                SelectionKey key = keys.next();
                keys.remove();
                if (!key.isValid()) {
                    continue;
                }
                if (key.isAcceptable()) {
                    accept(key);
                } else if (key.isReadable()) {
                    read(key);
                }
            }
        }
    }

    private void accept(SelectionKey key) throws IOException {
        ServerSocketChannel serverSocketChannel = (ServerSocketChannel) key.channel();
        SocketChannel channel = serverSocketChannel.accept();
        channel.configureBlocking(false);
        Socket socket = channel.socket();
        SocketAddress address = socket.getRemoteSocketAddress();
        logger.info("Connected by " + address);
        dataMapper.add(channel);
        channel.register(this.selector, SelectionKey.OP_READ);
        broadcast("New user: " + address + "\n");
    }

    private void read(SelectionKey key) throws IOException {
        SocketChannel socketChannel = (SocketChannel) key.channel();

        ByteBuffer byteBuffer = ByteBuffer.allocate(1024);
        int numRead = socketChannel.read(byteBuffer);
        if (numRead == -1) {
            dataMapper.remove(socketChannel);
            Socket socket = socketChannel.socket();
            SocketAddress address = socket.getRemoteSocketAddress();
            broadcast("User " + address + " left" + "\n");
            logger.info("Connection closed by " + address);
            socketChannel.close();
            key.cancel();
            return;
        }

        byte[] data = new byte[numRead];
        System.arraycopy(byteBuffer.array(), 0, data, 0, numRead);
        String message = new String(data);
        broadcast(address + ": " + message);
        logger.info(address + ": " + message);
    }

    private void broadcast(String message) throws IOException {
        ByteBuffer byteBuffer = ByteBuffer.allocate(1024);
        byteBuffer.put(message.getBytes());
        byteBuffer.flip();
        this.dataMapper.forEach(socketChannel -> {
            try {
                socketChannel.write(byteBuffer);
            } catch (IOException e) {
                e.printStackTrace();
            }
            byteBuffer.flip();
        });
    }
}
