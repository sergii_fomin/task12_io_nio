package Game.model;

import java.io.Serializable;

public class Droid implements Serializable {

    private String name;
    private int strength;
    private int damage;
    private int accuracy;

    public int getStrength() {
        return strength;
    }

    public void setStrength(int strength) {
        this.strength = strength;
    }

    public int getAccuracy() {
        return accuracy;
    }

    public void setAccuracy(int accuracy) {
        this.accuracy = accuracy;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getDamage() {
        return damage;
    }

    public void setDamage(int damage) {
        this.damage = damage;
    }

    @Override
    public String toString() {
        return "Droid{" +
                "name='" + name + '\'' +
                ", strength=" + strength +
                ", damage=" + damage +
                ", accuracy=" + accuracy +
                '}';
    }
}
