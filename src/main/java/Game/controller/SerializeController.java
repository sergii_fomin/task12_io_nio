package Game.controller;

public interface SerializeController {

    void serialize();
    void deserialize();
}
