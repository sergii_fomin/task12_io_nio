package Game.controller.impl;

import Game.controller.CommentController;
import Game.service.GetComment;

import java.util.List;
import java.util.Scanner;

public class CommentControllerImpl implements CommentController {

    private GetComment getComment;

    public CommentControllerImpl(GetComment getComment) {
        this.getComment = getComment;
    }

    @Override
    public void getComments() {
        Scanner scanner = new Scanner(System.in);
        String filename = scanner.next();
        List<String> comments = getComment.getCommentsFromFile(filename);
        for (String comment : comments) {
            System.out.println(comment);
        }
    }
}
