package Game.controller.impl;

import Game.controller.SerializeController;
import Game.model.Droid;
import Game.model.Ship;
import Game.service.Serialization;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class SerializeControllerImpl implements SerializeController {
    private Serialization serialization;

    public SerializeControllerImpl(Serialization serialization) {
        this.serialization = serialization;
    }

    @Override
    public void serialize() {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Enter ship name them capacity: ");
        Ship ship = new Ship();
        String name = scanner.nextLine();
        ship.setName(name);
        int capacity = scanner.nextInt();
        ship.setCapacity(capacity);

        List<Droid> droids = new ArrayList<>();

        for (int i = 0; i < capacity; ++i) {
            System.out.println("Enter droid name then damage: ");
            Droid droid = new Droid();
            String droidName = scanner.next();
            droid.setName(droidName);
            int droidDamage = scanner.nextInt();
            droid.setDamage(droidDamage);
            droids.add(droid);
        }
        ship.setDroids(droids);
        System.out.println("Enter file path to serialization:");
        String filepath = scanner.next();
        serialization.writeObject(filepath, ship);
    }


    @Override
    public void deserialize() {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Enter file path to deserialization:");
        String filepath = scanner.next();
        Object object = serialization.readObject(filepath);
        System.out.println(object);
    }
}
