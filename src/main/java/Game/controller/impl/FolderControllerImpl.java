package Game.controller.impl;

import Game.controller.FolderController;
import Game.service.GetFolder;

import java.io.File;
import java.util.List;
import java.util.Scanner;

public class FolderControllerImpl implements FolderController {

    private GetFolder getFolder;

    public FolderControllerImpl(GetFolder getFolder) {
        this.getFolder = getFolder;
    }

    @Override
    public void displayFolder() {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Enter file path to folders:");
        String folderPath = scanner.next();
        printFolderInformation(getFolder.getFolderContent(folderPath));
    }

    @Override
    public void cdFolder() {
        Scanner scanner = new Scanner(System.in);
        String folderPath = scanner.next();
        List<File> files = getFolder.getFolderContent(folderPath);
        StringBuilder currentFolder = new StringBuilder(folderPath);
        String newFolder;
        printFolderInformation(files);
        do {
            newFolder = scanner.next();
            printFolderInformation(getFolder.getFolderContent(currentFolder + newFolder));
            currentFolder.append(newFolder);
        } while (!newFolder.equals("q"));
    }

    private void printFolderInformation(List<File> files) {
        for (File file : files) {
            if (file.isFile()) {
                System.out.println("File " + file.getName());
                System.out.println("Readable = " + file.canRead());
                System.out.println("Executable = " + file.canExecute());
                System.out.println("Writable = " + file.canWrite());
            } else if (file.isDirectory()) {
                System.out.println("Folder " + file.getName());
            }
        }
    }
}
